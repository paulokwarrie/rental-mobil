<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice extends CI_Controller {

	private $header = "template/header";
	private $footer = "template/footer";
	private $path;
	private $data;
	
	public function __construct(){
		parent::__construct();
		$this->data['title'] = 'Invoice';
	}
	
	public function index() {
		// get invoice data
		$sql='SELECT *,DATE_ADD(tgl_terbit,INTERVAL 15 DAY) AS jth_tempo FROM invoice';
		$OInvoice = $this->db->query($sql);
		if($OInvoice->num_rows() > 0) {
			// setiap invoice memiliki -> 1 kontrak, >1 pembayaran
			foreach($OInvoice->result() as $invoice):
				// get total_pembayaran
				$sql='SELECT SUM(jumlah_pembayaran) AS total_pembayaran FROM pembayaran WHERE id_invoice='.$invoice->id;
				$OPembayaran = $this->db->query($sql);
				foreach($OPembayaran->result() as $pembayaran) $invoice->total_pembayaran = $pembayaran->total_pembayaran;

				// get kontrak data
				$OKontrak = $this->kontrak_model->get($invoice->id_kontrak);
				if($OKontrak->num_rows() > 0) {
					// setiap kontrak memiliki -> >1 mobil, 1 pelanggan
					foreach($OKontrak->result() as $kontrak):
						// get >1 mobil data
						$sql='SELECT m.merk_jenis_type, m.no_polisi, m.harga_sewa_per_bulan FROM kontrak_to_mobil ktm, mobil m, kontrak k WHERE ktm.id_kontrak='.$kontrak->id.' AND k.id=ktm.id_kontrak AND ktm.id_mobil=m.id';
						$OMobil = $this->db->query($sql);
						$invoice->mobil = $OMobil;

						// get pelanggan
						$OPelanggan = $this->pelanggan_model->get($kontrak->id_pelanggan);
						foreach($OPelanggan->result() as $pelanggan){
							$invoice->pelanggan_id = $pelanggan->id;
							$invoice->pelanggan_nama_perusahaan = $pelanggan->nama_perusahaan;
						}

						// get jumlah_penjualan
						$total=0;
						foreach($OMobil->result_array() as $mobil) $total = $total + $mobil['harga_sewa_per_bulan'];
						$invoice->jumlah_penjualan = $total+($total*0.1);

						// get kekurangan
						$invoice->kekurangan = $invoice->jumlah_penjualan-$invoice->total_pembayaran;
					endforeach;

					// get ktp -> periode_sewa
					$OKTP = $this->kontrak_to_periode_model->get($invoice->id_kontrak_to_periode);
					foreach($OKTP->result() as $ktp){
						$invoice->periode_sewa = date('d-m-Y',strtotime($ktp->awal_periode))." s/d ".date('d-m-Y',strtotime($ktp->akhir_periode));
					}

					$invoice->kontrak = $OKontrak;
				}
			endforeach;
			$this->data['invoice']=$OInvoice;
		}

		// get kontrak data
		$sql='SELECT k.id,k.no_kontrak,k.id_pelanggan,DATE_FORMAT(k.awal_sewa, "%d-%m-%Y") AS awal_sewa,DATE_FORMAT(k.akhir_sewa, "%d-%m-%Y") AS akhir_sewa,p.nama_perusahaan FROM pelanggan p, kontrak k WHERE k.keterangan IS NULL AND p.id=k.id_pelanggan AND (SELECT COUNT(id_kontrak) FROM kontrak_to_periode WHERE id_kontrak=k.id)!=(SELECT COUNT(id_kontrak) FROM invoice WHERE id_kontrak=k.id) ORDER BY k.no_kontrak';
		$OKontrak = $this->db->query($sql);
		if($OKontrak->num_rows() > 0) $this->data['kontrak'] = $OKontrak;

		// set no_invoice
		$sql='SELECT no_invoice, LEFT(no_invoice,3) AS nomor FROM invoice WHERE RIGHT(no_invoice,7)=DATE_FORMAT(CURDATE(),"%m-%Y") ORDER BY no_invoice DESC LIMIT 1';
		$OInvoice = $this->db->query($sql);
		if($OInvoice->num_rows() > 0) {
			foreach($OInvoice->result() as $invoice):
				$currNum = ltrim($invoice->nomor, '0');
				$nextNum = sprintf("%03d", $currNum + 1);
				$this->data['no_invoice'] = $nextNum.'/INV/'.date('m-Y');
			endforeach;
		} else $this->data['no_invoice'] = '001/INV/'.date('m-Y');

		$this->view('invoice_view');
	}

	public function add(){
		$this->form_validation->set_rules('no_invoice', 'No. Invoice', 'required|is_unique[invoice.no_invoice]');
		if($this->form_validation->run()) {
			// insert invoice
			$invoice=array(
				'no_invoice' => $this->input->post('no_invoice'),
				'id_kontrak' => $this->input->post('id_kontrak'),
				'id_kontrak_to_periode' => $this->input->post('id_kontrak_to_periode'),
				'tgl_terbit' => $this->input->post('tgl_terbit'),
			);
			if($this->invoice_model->add($invoice) == "Success") $this->session->set_flashdata('success', 'Add Record Success.');
			else $this->session->set_flashdata('fail', 'Add Record Failed.');
			redirect('invoice');
		} else echo validation_errors();
	}

	public function show_invoice($id_invoice=false){
		if($id_invoice) {
			// get invoice data
			$OInvoice = $this->invoice_model->get($id_invoice);
			if($OInvoice->num_rows() > 0) {
				// setiap invoice memiliki 1 kontrak
				foreach($OInvoice->result() as $invoice) {
					// get kontrak data
					$OKontrak = $this->kontrak_model->get($invoice->id_kontrak);
					// setiap kontrak memiliki >1 mobil
					foreach($OKontrak->result() as $kontrak){
						$sql='SELECT ktm.id, m.merk_jenis_type, m.no_polisi, m.harga_sewa_per_bulan FROM kontrak_to_mobil ktm, mobil m, kontrak k WHERE ktm.id_kontrak='.$kontrak->id.' AND k.id=ktm.id_kontrak AND ktm.id_mobil=m.id';
						$OMobil = $this->db->query($sql);
						$kontrak->mobil = $OMobil;
					}
					$invoice->kontrak = $OKontrak;
				}
				foreach($OInvoice->result() as $invoice) {
					foreach($invoice->kontrak->result() as $kontrak) {
						echo '<div class="modal-header">';
							echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
							echo '<h4 class="modal-title" id="myModalLabel">Data Mobil</h4>';
						echo '</div>';
						echo '<div class="modal-body">';
							echo 'Kontrak      : <b>'.$kontrak->no_kontrak.'</b><br/>';
							echo 'Periode Sewa : <b>'.date('d-m-Y',strtotime($kontrak->awal_sewa)).' s/d '.date('d-m-Y',strtotime($kontrak->akhir_sewa)).'</b><br/><br/>';
							echo '<table class="table table-bordered">';
								echo '<thead>';
									echo '<tr>';
										echo '<th>Merk/Jenis/Type</th>';
										echo '<th>No. Polisi</th>';
										echo '<th>Harga Sewa Per Bulan</th>';
									echo '</tr>';
								echo '</thead>';
								echo '<tbody>';
									foreach($kontrak->mobil->result() as $mobil):
									echo '<tr>';
										echo '<td>'.$mobil->merk_jenis_type.'</td>';
										echo '<td>'.$mobil->no_polisi.'</td>';
										echo '<td style="text-align:right">Rp. '.number_format($mobil->harga_sewa_per_bulan,0, ',', '.').'</td>';
									echo '</tr>';
									endforeach;
								echo '</tbody>';
							echo '</table>';
						echo '</div>';
						echo '<div class="modal-footer">';
							echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
						echo '</div>';
					}
				}
			} else echo "Data Invoice tidak ada.";
		} else show_404();
	}

	public function show_pelanggan($id_invoice=false){
		if($id_invoice) {
			// get invoice data
			$OInvoice = $this->invoice_model->get($id_invoice);
			if($OInvoice->num_rows() > 0) {
				// setiap invoice memiliki 1 kontrak
				foreach($OInvoice->result() as $invoice) {
					// get kontrak data
					$OKontrak = $this->kontrak_model->get($invoice->id_kontrak);
					// setiap kontrak memiliki 1 pelanggan
					foreach($OKontrak->result() as $kontrak){
						$OPelanggan = $this->pelanggan_model->get($kontrak->id_pelanggan);
						$kontrak->pelanggan = $OPelanggan;
					}
					$invoice->kontrak = $OKontrak;
				}
				foreach($OInvoice->result() as $invoice) {
					foreach($invoice->kontrak->result() as $kontrak) {
						echo '<div class="modal-header">';
							echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
							echo '<h4 class="modal-title" id="myModalLabel">Data Pelanggan</h4>';
						echo '</div>';
						echo '<div class="modal-body">';
							echo 'Kontrak      : <b>'.$kontrak->no_kontrak.'</b><br/>';
							echo 'Periode Sewa : <b>'.date('d-m-Y',strtotime($kontrak->awal_sewa)).' s/d '.date('d-m-Y',strtotime($kontrak->akhir_sewa)).'</b><br/><br/>';
							echo '<table class="table table-bordered">';
								echo '<thead>';
									echo '<tr>';
										echo '<th>Nama Penanggung Jawab</th>';
										echo '<th>No. Telp Perusahaan</th>';
										echo '<th>Kota</th>';
									echo '</tr>';
								echo '</thead>';
								echo '<tbody>';
									foreach($kontrak->pelanggan->result() as $pelanggan):
									echo '<tr>';
										echo '<td>'.$pelanggan->nama_penanggung_jawab.'</td>';
										echo '<td>'.$pelanggan->no_telp_perusahaan.'</td>';
										echo '<td>'.$pelanggan->kota.'</td>';
									echo '</tr>';
									endforeach;
								echo '</tbody>';
							echo '</table>';
						echo '</div>';
						echo '<div class="modal-footer">';
							echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
						echo '</div>';
					}
				}
			} else echo "Data Pelanggan tidak ada.";
		} else show_404();
	}

	public function show_pembayaran_form($id_invoice=false){
		if($id_invoice) {
			$id=decryptURL($id_invoice);
			// get invoice data
			$OInvoice = $this->invoice_model->get($id);
			if($OInvoice->num_rows() > 0) {
				// setiap invoice memiliki >1 pembayaran, 1 kontrak
				foreach($OInvoice->result() as $invoice) {
					// get pembayaran data
					$sql='SELECT * FROM pembayaran WHERE id_invoice='.$invoice->id.' ORDER BY tgl_pembayaran ASC';
					$OPembayaran = $this->db->query($sql);
					if($OPembayaran->num_rows() > 0) {
						$invoice->pembayaran = $OPembayaran;
					}
					// get total_pembayaran
					$sql='SELECT SUM(jumlah_pembayaran) AS total_pembayaran FROM pembayaran WHERE id_invoice='.$invoice->id;
					$OPembayaran = $this->db->query($sql);
					foreach($OPembayaran->result() as $pembayaran) $invoice->total_pembayaran = $pembayaran->total_pembayaran;

					// get kontrak data
					$OKontrak = $this->kontrak_model->get($invoice->id_kontrak);
					// setiap kontrak memiliki banyak mobil
					foreach($OKontrak->result() as $kontrak){
						// get total_penjualan
						$sql = 'SELECT SUM(m.harga_sewa_per_bulan) AS total_penjualan FROM mobil m, kontrak_to_mobil ktm WHERE ktm.id_mobil=m.id AND ktm.id_kontrak='.$kontrak->id;
						$OMobil = $this->db->query($sql);
						foreach($OMobil->result() as $mobil) $invoice->total_penjualan = $mobil->total_penjualan+($mobil->total_penjualan*0.1);
						// get kekurangan
						$invoice->kekurangan = $invoice->total_penjualan-$invoice->total_pembayaran;
					}
				}
				$this->data['invoice'] = $OInvoice;
				$this->load->view($this->path . '/pembayaran_form', $this->data);
			} else echo "Data Pembayaran tidak ada.";
		} else show_404();
	}

	public function delete($id){
		$id=decryptURL($id);
		if($this->invoice_model->delete($id) == "Success")
			$this->session->set_flashdata('success', 'Delete Success.');
		else
			$this->session->set_flashdata('fail', 'Delete Failed.');
		redirect('invoice');
	}

	public function print_view($id_invoice=false){
		if($id_invoice){
			$this->data['title'] = 'Print';

			$id=decryptURL($id_invoice);

			// get invoice data
			$sql='SELECT *,DATE_ADD(tgl_terbit,INTERVAL 15 DAY) AS jth_tempo FROM invoice WHERE id='.$id;
			$OInvoice = $this->db->query($sql);
			if($OInvoice->num_rows() > 0) {
				// setiap invoice memiliki -> 1 kontrak
				foreach($OInvoice->result() as $invoice):
					// get kontrak data
					$OKontrak = $this->kontrak_model->get($invoice->id_kontrak);
					if($OKontrak->num_rows() > 0) {
						// setiap kontrak memiliki -> >1 mobil, 1 pelanggan
						foreach($OKontrak->result() as $kontrak):
							// get >1 mobil data
							$sql='SELECT m.merk_jenis_type, m.no_polisi, m.harga_sewa_per_bulan FROM kontrak_to_mobil ktm, mobil m, kontrak k WHERE ktm.id_kontrak='.$kontrak->id.' AND k.id=ktm.id_kontrak AND ktm.id_mobil=m.id';
							$OMobil = $this->db->query($sql);
							$invoice->mobil = $OMobil;

							// get pelanggan
							$OPelanggan = $this->pelanggan_model->get($kontrak->id_pelanggan);
							foreach($OPelanggan->result() as $pelanggan) $kontrak->pelanggan = $OPelanggan;

							// get jumlah_pembayaran
							$total=0;
							foreach($OMobil->result_array() as $mobil) $total = $total + $mobil['harga_sewa_per_bulan'];
							$invoice->jumlah_pembayaran = $total;
						endforeach;

						// get ktp -> periode_sewa
						$OKTP = $this->kontrak_to_periode_model->get($invoice->id_kontrak_to_periode);
						foreach($OKTP->result() as $ktp){
							$invoice->periode_sewa = date('d/m/Y',strtotime($ktp->awal_periode))." s/d ".date('d/m/Y',strtotime($ktp->akhir_periode));
						}

						$invoice->kontrak = $OKontrak;
					}
				endforeach;
				$this->data['invoice']=$OInvoice;
			}

			$this->load->view($this->path . '/' . 'print', $this->data);
			$this->load->view($this->footer, $this->data);
		} else show_404();
	}
	
	private function view($page){
		$this->load->view($this->header, $this->data);
		$this->load->view($this->path . '/' . $page, $this->data);
		$this->load->view($this->footer, $this->data);
    }
}
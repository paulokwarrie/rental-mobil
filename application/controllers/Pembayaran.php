<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

	private $header = "template/header";
	private $footer = "template/footer";
	private $path;
	private $data;
	
	public function __construct(){
		parent::__construct();
		$this->data['title'] = 'Pembayaran';
	}
	
	public function index() {
		show_404();
	}
	
	public function add(){
		$input=$this->input->post();
		if($this->pembayaran_model->add($input) == "Success"){
			// get invoice data
			$OInvoice = $this->invoice_model->get($this->input->post('id_invoice'));
			// setiap invoice memiliki >1 pembayaran, 1 kontrak
			foreach($OInvoice->result() as $invoice) {
				// get kontrak data
				$OKontrak = $this->kontrak_model->get($invoice->id_kontrak);
				// setiap kontrak memiliki banyak mobil
				foreach($OKontrak->result() as $kontrak){
					// get mobil data
					$sql = 'SELECT SUM(m.harga_sewa_per_bulan) AS total_penjualan FROM mobil m, kontrak_to_mobil ktm WHERE ktm.id_mobil=m.id AND ktm.id_kontrak='.$kontrak->id;
					$OMobil = $this->db->query($sql);
					foreach($OMobil->result() as $mobil) $total_penjualan = $mobil->total_penjualan;
				}
				// get pembayaran data
				$sql='SELECT SUM(jumlah_pembayaran) AS total_pembayaran FROM pembayaran WHERE id_invoice='.$invoice->id.' ORDER BY tgl_pembayaran ASC';
				$OPembayaran = $this->db->query($sql);
				foreach($OPembayaran->result() as $pembayaran) $total_pembayaran = $pembayaran->total_pembayaran;
			}

			// cek status invoice
			if($total_pembayaran>=$total_penjualan) $status="Paid";
			else if($total_pembayaran<$total_penjualan && $total_pembayaran>0) $status="Incomplete";
			else $status="Unpaid";

			// edit invoice status
			if($this->invoice_model->edit($this->input->post('id_invoice'),array('status' => $status)) == "Success") {}
			$this->session->set_flashdata('success', 'Pembayaran Success.');
		}
		else
			$this->session->set_flashdata('fail', 'Pembayaran Failed.');
		redirect('invoice');
	}

	public function delete($id){
		$id=decryptURL($id);
		// get invoice id
		$OPembayaran = $this->pembayaran_model->get($id);
		foreach($OPembayaran->result() as $pembayaran) $id_invoice = $pembayaran->id_invoice;

		if($this->pembayaran_model->delete($id) == "Success"){

			// get invoice data
			$OInvoice = $this->invoice_model->get($id_invoice);
			// setiap invoice memiliki >1 pembayaran, 1 kontrak
			foreach($OInvoice->result() as $invoice) {
				// get kontrak data
				$OKontrak = $this->kontrak_model->get($invoice->id_kontrak);
				// setiap kontrak memiliki banyak mobil
				foreach($OKontrak->result() as $kontrak){
					// get total_penjualan
					$sql = 'SELECT SUM(m.harga_sewa_per_bulan) AS total_penjualan FROM mobil m, kontrak_to_mobil ktm WHERE ktm.id_mobil=m.id AND ktm.id_kontrak='.$kontrak->id;
					$OMobil = $this->db->query($sql);
					foreach($OMobil->result() as $mobil) $total_penjualan = $mobil->total_penjualan;
				}
				// get total_pembayaran
				$sql='SELECT SUM(jumlah_pembayaran) AS total_pembayaran FROM pembayaran WHERE id_invoice='.$invoice->id.' ORDER BY tgl_pembayaran ASC';
				$OPembayaran = $this->db->query($sql);
				foreach($OPembayaran->result() as $pembayaran) $total_pembayaran = $pembayaran->total_pembayaran;
			}

			// cek status invoice
			if($total_pembayaran>=$total_penjualan) $status="Paid";
			else if($total_pembayaran<$total_penjualan && $total_pembayaran>0) $status="Incomplete";
			else $status="Unpaid";

			// edit invoice status
			if($this->invoice_model->edit($id_invoice,array('status' => $status)) == "Success") {}
			$this->session->set_flashdata('success', 'Delete Success.');
		}
		else
			$this->session->set_flashdata('fail', 'Delete Failed.');
		redirect('invoice');
	}
	
	private function view($page){
		$this->load->view($this->header, $this->data);
		$this->load->view($this->path . '/' . $page, $this->data);
		$this->load->view($this->footer, $this->data);
    }
}
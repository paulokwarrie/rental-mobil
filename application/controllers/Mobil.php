<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mobil extends CI_Controller {

	private $header = "template/header";
	private $footer = "template/footer";
	private $path;
	private $data;
	
	public function __construct(){
		parent::__construct();
		$this->data['title'] = 'Mobil';
	}
	
	public function index() {
		$OMobil = $this->mobil_model->get();
		if($OMobil->num_rows() > 0) $this->data['mobil'] = $OMobil;
		$this->view('mobil_view');
	}
	
	public function add(){
		$input=$this->input->post();
		if($this->mobil_model->add($input) == "Success")
			$this->session->set_flashdata('success', 'Add Record Success.');
		else
			$this->session->set_flashdata('fail', 'Add Record Failed.');
		redirect('mobil');
	}

	public function edit($id){
		$id=decryptURL($id);
		$input=$this->input->post();
		if($this->mobil_model->edit($id,$input) == "Success")
			$this->session->set_flashdata('success', 'Edit Success.');
		else
			$this->session->set_flashdata('fail', 'Edit Failed.');
		redirect('mobil');
	}

	public function delete($id){
		$id=decryptURL($id);
		if($this->mobil_model->delete($id) == "Success")
			$this->session->set_flashdata('success', 'Delete Success.');
		else
			$this->session->set_flashdata('fail', 'Delete Failed.');
		redirect('mobil');
	}

	public function edit_form($id){
		$this->data['title'] = 'Edit Record';

		$id=decryptURL($id);
		$OMobil = $this->mobil_model->get($id);
		if($OMobil->num_rows() > 0) {
			foreach($OMobil->result_array() as $item) {
				$this->data['mobil'] = $item;
			}
		}
		else redirect('mobil');
		$this->view('mobil_edit');
	}

	public function getAvailableCar($awal_sewa=false,$akhir_sewa=false) {
		// if(! $this->input->is_ajax_request()) show_404();
		// else {
			// get kontrak data
			$OKontrak = $this->kontrak_model->get();
			if($OKontrak->num_rows() > 0) {
				// setiap kontrak memiliki >1 mobil
				foreach($OKontrak->result() as $kontrak) {
					// cek if available
					// if available
					if($awal_sewa > $kontrak->akhir_sewa || $akhir_sewa < $kontrak->awal_sewa) {
						$status = "Available";
						echo "KONTRAK ID : ".$kontrak->id;
						echo "<br/>Date DB : <br/>";
						echo $kontrak->awal_sewa." s/d ";
						echo $kontrak->akhir_sewa."<br/>";
						echo "Date IN : <br/>";
						echo $awal_sewa." s/d ";
						echo $akhir_sewa."<br/>";
						echo "available<br/>";
						// get mobil
						// $sql='SELECT m.* FROM mobil m, kontrak_to_mobil ktm WHERE ktm.id_kontrak='.$kontrak->id_kontrak;
						// $OKTP = $this->mobil_model->get();
					}
				}
			} else {
				echo "fail";
				// get all mobil
				// $sql='SELECT id, CONCAT(DATE_FORMAT(awal_periode, "%d-%m-%Y"), " s/d ", DATE_FORMAT(akhir_periode, "%d-%m-%Y")) AS text FROM kontrak_to_periode WHERE id NOT IN (SELECT id_kontrak_to_periode FROM invoice WHERE id_kontrak='.$id_kontrak.') AND id_kontrak='.$id_kontrak;
				// $OKTP = $this->db->query($sql);
				// echo json_encode($OKTP->result_array());
			}
		// }
	}
	
	private function view($page){
		$this->load->view($this->header, $this->data);
		$this->load->view($this->path . '/' . $page, $this->data);
		$this->load->view($this->footer, $this->data);
    }
}
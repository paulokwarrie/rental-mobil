<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

	private $header = "template/header";
	private $footer = "template/footer";
	private $path;
	private $data;
	
	public function __construct(){
		parent::__construct();
		$this->data['title'] = 'Pelanggan';
	}
	
	public function index() {
		$OPelanggan = $this->pelanggan_model->get();
		if($OPelanggan->num_rows() > 0) $this->data['pelanggan'] = $OPelanggan;
		$this->view('pelanggan_view');
	}
	
	public function add(){
		$input=$this->input->post();
		if($this->pelanggan_model->add($input) == "Success")
			$this->session->set_flashdata('success', 'Add Record Success.');
		else
			$this->session->set_flashdata('fail', 'Add Record Failed.');
		redirect('pelanggan');
	}

	public function edit($id){
		$id=decryptURL($id);
		$input=$this->input->post();
		if($this->pelanggan_model->edit($id,$input) == "Success")
			$this->session->set_flashdata('success', 'Edit Success.');
		else
			$this->session->set_flashdata('fail', 'Edit Failed.');
		redirect('pelanggan');
	}

	public function delete($id){
		$id=decryptURL($id);
		if($this->pelanggan_model->delete($id) == "Success")
			$this->session->set_flashdata('success', 'Delete Success.');
		else
			$this->session->set_flashdata('fail', 'Delete Failed.');
		redirect('pelanggan');
	}

	public function edit_form($id){
		$this->data['title'] = 'Edit Record';

		$id=decryptURL($id);
		$OPelanggan = $this->pelanggan_model->get($id);
		if($OPelanggan->num_rows() > 0) {
			foreach($OPelanggan->result_array() as $item) {
				$this->data['pelanggan'] = $item;
			}
		}
		else show_404();
		$this->view('pelanggan_edit');
	}
	
	private function view($page){
		$this->load->view($this->header, $this->data);
		$this->load->view($this->path . '/' . $page, $this->data);
		$this->load->view($this->footer, $this->data);
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontrak extends CI_Controller {

	private $header = "template/header";
	private $footer = "template/footer";
	private $path;
	private $data;
	
	public function __construct(){
		parent::__construct();
		$this->data['title'] = 'Kontrak';
	}
	
	public function index() {
		// get kontrak data
		$sql='SELECT k.id, k.no_kontrak, p.nama_perusahaan, p.nama_penanggung_jawab, p.no_telp_perusahaan, p.kota, k.awal_sewa, k.akhir_sewa, k.keterangan FROM kontrak k, pelanggan p WHERE k.id_pelanggan=p.id';
		$OKontrak = $this->db->query($sql);
		if($OKontrak->num_rows() > 0) {
			// setiap kontrak memiliki
			foreach($OKontrak->result() as $kontrak):
				// get mobil data
				$sql='SELECT ktm.id, m.merk_jenis_type, m.no_polisi, m.harga_sewa_per_bulan FROM kontrak_to_mobil ktm, mobil m, kontrak k WHERE ktm.id_kontrak='.$kontrak->id.' AND k.id=ktm.id_kontrak AND ktm.id_mobil=m.id';
				$OMobil = $this->db->query($sql);
				$kontrak->mobil = $OMobil;
				// get total harga sewa
				$total=0;
				foreach($OMobil->result_array() as $mobil){
					$total = $total + ( $mobil['harga_sewa_per_bulan'] * periodeSewa($kontrak->awal_sewa,$kontrak->akhir_sewa) );
				}
				$kontrak->total_harga_sewa = $total;
			endforeach;
			$this->data['kontrak']=$OKontrak;
		}
		// get pelanggan data
		$OPelanggan = $this->pelanggan_model->get();
		if($OPelanggan->num_rows() > 0) $this->data['pelanggan'] = $OPelanggan;
		// get mobil data
		$OMobil = $this->mobil_model->get();
		if($OMobil->num_rows() > 0) $this->data['mobil'] = $OMobil;
		// set no_kontrak
		$sql='SELECT no_kontrak, LEFT(no_kontrak,3) AS nomor FROM kontrak WHERE RIGHT(no_kontrak,7)=DATE_FORMAT(CURDATE(),"%m-%Y") ORDER BY no_kontrak DESC LIMIT 1';
		$OKontrak = $this->db->query($sql);
		if($OKontrak->num_rows() > 0) {
			foreach($OKontrak->result() as $kontrak):
				$currNum = ltrim($kontrak->nomor, '0');
				$nextNum = sprintf("%03d", $currNum + 1);
				$this->data['no_kontrak'] = $nextNum.'/RM/'.date('m-Y');
			endforeach;
		} else $this->data['no_kontrak'] = '001/RM/'.date('m-Y');
		$this->view('kontrak_view');
	}

	public function show_mobil($id_kontrak=false) {
		if($id_kontrak) {
			// get mobil data
			$sql='SELECT ktm.id, m.merk_jenis_type, m.no_polisi, m.harga_sewa_per_bulan FROM kontrak_to_mobil ktm, mobil m, kontrak k WHERE ktm.id_kontrak='.$id_kontrak.' AND k.id=ktm.id_kontrak AND ktm.id_mobil=m.id';
			$OMobil = $this->db->query($sql);
			if($OMobil->num_rows() > 0) {
				echo '<div class="modal-header">';
					echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					echo '<h4 class="modal-title" id="myModalLabel">Data Mobil</h4>';
				echo '</div>';
				echo '<div class="modal-body table-responsive">';
					echo '<table class="table table-bordered">';
						echo '<thead>';
							echo '<tr>';
								echo '<th>Merk/Jenis/Type</th>';
								echo '<th>No. Polisi</th>';
								echo '<th>Harga Sewa Per Bulan</th>';
							echo '</tr>';
						echo '</thead>';
						echo '<tbody>';
							foreach($OMobil->result_array() as $mobil):
							echo '<tr>';
								echo '<td>'.$mobil['merk_jenis_type'].'</td>';
								echo '<td>'.$mobil['no_polisi'].'</td>';
								echo '<td style="text-align:right">Rp. '.number_format($mobil['harga_sewa_per_bulan'],0, ',', '.').'</td>';
							echo '</tr>';
							endforeach;
						echo '</tbody>';
					echo '</table>';
				echo '</div>';
				echo '<div class="modal-footer">';
					echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
				echo '</div>';
			} else echo "Data Mobil tidak ada.";
		} else show_404();
	}

	public function show_pelanggan($id_kontrak=false) {
		if($id_kontrak) {
			// get pelanggan data
			$sql='SELECT p.nama_perusahaan, p.nama_penanggung_jawab, p.no_telp_perusahaan, p.kota FROM kontrak k, pelanggan p WHERE k.id_pelanggan=p.id AND k.id='.$id_kontrak;
			$OPelanggan = $this->db->query($sql);
			if($OPelanggan->num_rows() > 0) {
				echo '<div class="modal-header">';
					echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					echo '<h4 class="modal-title" id="myModalLabel">Data Pelanggan</h4>';
				echo '</div>';
				echo '<div class="modal-body table-responsive">';
					echo '<table class="table table-bordered">';
						echo '<thead>';
							echo '<tr>';
								echo '<th>Nama Penanggung Jawab</th>';
								echo '<th>No. Telp Perusahaan</th>';
								echo '<th>Kota</th>';
							echo '</tr>';
						echo '</thead>';
						echo '<tbody>';
							foreach($OPelanggan->result_array() as $pelanggan):
							echo '<tr>';
								echo '<td>'.$pelanggan['nama_penanggung_jawab'].'</td>';
								echo '<td>'.$pelanggan['no_telp_perusahaan'].'</td>';
								echo '<td>'.$pelanggan['kota'].'</td>';
							echo '</tr>';
							endforeach;
						echo '</tbody>';
					echo '</table>';
				echo '</div>';
				echo '<div class="modal-footer">';
					echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
				echo '</div>';
			} else echo "Data Pelanggan tidak ada.";
		} else show_404();
	}
	
	public function show_status($id_kontrak=false) {
		if($id_kontrak) {
			// get kontrak data
			$sql='SELECT awal_sewa, akhir_sewa FROM kontrak WHERE id='.$id_kontrak;
			$OKontrak = $this->db->query($sql);
			if($OKontrak->num_rows() > 0) {
				foreach($OKontrak->result() as $kontrak) $status = getStatus($kontrak->awal_sewa,$kontrak->akhir_sewa);
				echo '<div class="modal-header">';
					echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					echo '<h4 class="modal-title" id="myModalLabel">Status</h4>';
				echo '</div>';
				echo form_open('kontrak/edit_keterangan/'.encryptURL($id_kontrak), array('class' => 'form-horizontal'));
				echo '<div class="modal-body">';
					echo '<div class="form-group">';
						echo '<label for="status" class="col-sm-2 control-label">Status</label>';
						echo '<div class="col-sm-10">';
							echo '<select id="status" name="status" class="form-control status">';
								echo '<option value="'.$status.'">'.$status.'</option>';
								echo '<option value="Canceled">Canceled</option>';
							echo '</select>';
						echo '</div>';
					echo '</div>';
					echo '<div class="form-group">';
						echo '<label for="keterangan" class="col-sm-2 control-label">Keterangan</label>';
						echo '<div class="col-sm-10">';
							echo '<textarea id="keterangan" name="keterangan" class="form-control keterangan"></textarea>';
						echo '</div>';
					echo '</div>';
				echo '</div>';
				echo '<div class="modal-footer">';
					echo '<button type="submit" class="btn btn-primary">Edit</button>';
					echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
				echo '</div>';
				echo form_close();
			} else echo "Data Status tidak ada.";
		} else show_404();
	}

	public function show_keterangan($id_kontrak=false){
		if($id_kontrak) {
			// get kontrak data
			$sql='SELECT keterangan FROM kontrak WHERE id='.$id_kontrak;
			$OKontrak = $this->db->query($sql);
			if($OKontrak->num_rows() > 0) {
				foreach($OKontrak->result() as $kontrak) $keterangan = $kontrak->keterangan;
				echo '<div class="modal-header">';
					echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					echo '<h4 class="modal-title" id="myModalLabel">Keterangan</h4>';
				echo '</div>';
				echo '<div class="modal-body">';
					echo '<p>'.$keterangan.'</p>';
				echo '</div>';
				echo '<div class="modal-footer">';
					echo '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
				echo '</div>';
			} else echo "Data Keterangan tidak ada.";
		} else show_404();
	}

	public function getPeriode($id_kontrak=false){
		if(! $this->input->is_ajax_request()) show_404();
		else {
			$sql='SELECT id, CONCAT(DATE_FORMAT(awal_periode, "%d-%m-%Y"), " s/d ", DATE_FORMAT(akhir_periode, "%d-%m-%Y")) AS text FROM kontrak_to_periode WHERE id NOT IN (SELECT id_kontrak_to_periode FROM invoice WHERE id_kontrak='.$id_kontrak.') AND id_kontrak='.$id_kontrak;
			$OKTP = $this->db->query($sql);
			echo json_encode($OKTP->result_array());
		}
	}

	public function add(){
		$this->form_validation->set_rules('no_kontrak', 'No. Kontrak', 'required|is_unique[kontrak.no_kontrak]');
		if($this->form_validation->run()) {
			// insert kontrak
			$kontrak=array(
				'no_kontrak' => $this->input->post('no_kontrak'),
				'id_pelanggan' => $this->input->post('id_pelanggan'),
				'awal_sewa' => $this->input->post('awal_sewa'),
				'akhir_sewa' => $this->input->post('akhir_sewa')
			);
			if($this->kontrak_model->add($kontrak) == "Success") {
				// get id_kontrak
				$id_kontrak = $this->db->insert_id();
				// insert kontrak_to_mobil
				$ktm=array();
				foreach($this->input->post('id_mobil') as $id_mobil){
					$arr = array(
						'id_kontrak' => $id_kontrak,
						'id_mobil' => $id_mobil
					);
					array_push($ktm,$arr);
				}
				// insert kontrak_to_periode
				$awal_sewa = $this->input->post('awal_sewa');
				$akhir_sewa = $this->input->post('akhir_sewa');
				$jumlah_bulan = periodeSewa($awal_sewa,$akhir_sewa);

				$awal = date('Y-m-d',strtotime('+0 day', strtotime($awal_sewa)));
				$akhir = date('Y-m-d',strtotime('+29 day', strtotime($awal_sewa)));

				$ktp=array();
				for($i=1;$i<=$jumlah_bulan;$i++){
					$arr = array(
						'id_kontrak' => $id_kontrak,
						'awal_periode' => $awal,
						'akhir_periode' => $akhir
					);
					if($i == $jumlah_bulan) {
						$akhir = $this->input->post('akhir_sewa');
						$arr = array(
							'id_kontrak' => $id_kontrak,
							'awal_periode' => $awal,
							'akhir_periode' => $akhir
						);
					} else {
						$awal = date('Y-m-d',strtotime('+1 day', strtotime($akhir)));
						$akhir = date('Y-m-d',strtotime('+29 day', strtotime($awal)));
					}
					array_push($ktp,$arr);
				}
				if($this->kontrak_to_mobil_model->add_batch($ktm) == "Success" && $this->kontrak_to_periode_model->add_batch($ktp) == "Success")
					$this->session->set_flashdata('success', 'Add Record Success.');
				else $this->session->set_flashdata('fail', 'Add Record Failed.');
			} else $this->session->set_flashdata('fail', 'Add Record Failed.');
			redirect('kontrak');
		} else echo validation_errors();
	}

	public function edit($id){
		$id=decryptURL($id);

		// edit kontrak
		$kontrak=array(
			'no_kontrak' => $this->input->post('no_kontrak'),
			'id_pelanggan' => $this->input->post('id_pelanggan'),
			'awal_sewa' => $this->input->post('awal_sewa'),
			'akhir_sewa' => $this->input->post('akhir_sewa')
		);
		if($this->kontrak_model->edit($id,$kontrak) == "Success"){}

		// edit kontrak_to_mobil -> delete + add
		// delete
		if($this->kontrak_to_mobil_model->delete_where(array('id_kontrak' => $id)) == "Success"){}
		// add
		$ktm=array();
		foreach($this->input->post('id_mobil') as $id_mobil){
			$arr = array(
				'id_kontrak' => $id,
				'id_mobil' => $id_mobil
			);
			array_push($ktm,$arr);
		}

		// edit kontrak_to_mobil -> delete + add
		// delete
		if($this->kontrak_to_periode_model->delete_where(array('id_kontrak' => $id)) == "Success"){}
		// add
		$awal_sewa = $this->input->post('awal_sewa');
		$akhir_sewa = $this->input->post('akhir_sewa');
		$jumlah_bulan = periodeSewa($awal_sewa,$akhir_sewa);

		$awal = date('Y-m-d',strtotime('+0 day', strtotime($awal_sewa)));
		$akhir = date('Y-m-d',strtotime('+29 day', strtotime($awal_sewa)));

		$ktp=array();
		for($i=1;$i<=$jumlah_bulan;$i++){
			$arr = array(
				'id_kontrak' => $id,
				'awal_periode' => $awal,
				'akhir_periode' => $akhir
			);
			if($i == $jumlah_bulan) {
				$akhir = $this->input->post('akhir_sewa');
				$arr = array(
					'id_kontrak' => $id,
					'awal_periode' => $awal,
					'akhir_periode' => $akhir
				);
			} else {
				$awal = date('Y-m-d',strtotime('+1 day', strtotime($akhir)));
				$akhir = date('Y-m-d',strtotime('+29 day', strtotime($awal)));
			}
			array_push($ktp,$arr);
		}

		if($this->kontrak_to_mobil_model->add_batch($ktm) == "Success" && $this->kontrak_to_periode_model->add_batch($ktp) == "Success")
			$this->session->set_flashdata('success', 'Edit Success.');
		else $this->session->set_flashdata('fail', 'Edit Failed.');

		redirect('kontrak');
	}

	public function edit_keterangan($id){
		$id=decryptURL($id);
		// edit kontrak
		if($this->input->post('status')=="Canceled")
			$input=array( 'keterangan' => $this->input->post('keterangan') );
		else
			$input=array( 'keterangan' => null );
		if($this->kontrak_model->edit($id,$input) == "Success")
			$this->session->set_flashdata('success', 'Edit Success.');
		else
			$this->session->set_flashdata('fail', 'Edit Failed.');
		redirect('kontrak');
	}

	public function delete($id){
		$id=decryptURL($id);
		if($this->kontrak_model->delete($id) == "Success")
			$this->session->set_flashdata('success', 'Delete Success.');
		else
			$this->session->set_flashdata('fail', 'Delete Failed.');
		redirect('kontrak');
	}

	public function edit_form($id){
		$this->data['title'] = 'Edit Record';

		$id=decryptURL($id);
		// get kontrak data
		$OKontrak = $this->kontrak_model->get($id);
		if($OKontrak->num_rows() > 0) {
			foreach($OKontrak->result() as $kontrak) {
				// get pelanggan selected data
				$sql='SELECT p.id FROM pelanggan p, kontrak k WHERE k.id_pelanggan=p.id AND k.id='.$id;
				$OPelanggan = $this->db->query($sql);
				foreach($OPelanggan->result() as $pelanggan) $kontrak->pelangganSelected = $pelanggan->id;
				// get mobil selected data
				$sql='SELECT m.id FROM kontrak_to_mobil ktm, mobil m, kontrak k WHERE ktm.id_kontrak='.$id.' AND k.id=ktm.id_kontrak AND ktm.id_mobil=m.id';
				$OMobil = $this->db->query($sql);
				$kontrak->mobilSelected = $OMobil;
			} $this->data['kontrak'] = $OKontrak;
		} else show_404();

		// get pelanggan data
		$OPelanggan = $this->pelanggan_model->get();
		if($OPelanggan->num_rows() > 0) $this->data['pelanggan'] = $OPelanggan;
		// get mobil data
		$OMobil = $this->mobil_model->get();
		if($OMobil->num_rows() > 0) $this->data['mobil'] = $OMobil;

		$this->view('kontrak_edit');
	}
	
	private function view($page){
		$this->load->view($this->header, $this->data);
		$this->load->view($this->path . '/' . $page, $this->data);
		$this->load->view($this->footer, $this->data);
    }
}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function printConfirmationMsg($flashdataName, $alertType = 'success') {
	$CI =& get_instance();
	if($CI->session->flashdata($flashdataName)) {
		$recognizedAlertTypes = array('success', 'info', 'warning', 'danger');
		if(!in_array($alertType, $recognizedAlertTypes))
			$alertType = 'success';
		echo '<div class="alert fade in alert-'.$alertType.' alert-dismissible" role="alert">';
		echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
	  	echo $CI->session->flashdata($flashdataName);
	  	echo '</div>';
	}
}
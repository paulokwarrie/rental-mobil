<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function encryptURL($input){
	$CI =& get_instance();
	$enc=$CI->encrypt->encode($input);
	$enc=str_replace(array('+', '/', '='), array('-', '_', '~'), $enc);
	return $enc;
}

function decryptURL($input){
	$CI =& get_instance();
	$dec=str_replace(array('-', '_', '~'), array('+', '/', '='), $input);
	$dec=$CI->encrypt->decode($dec);
	return $dec;
}

function periodeSewa($awal,$akhir){
	$awal = strtotime($awal);
	$akhir = strtotime($akhir);
	$datediff = $akhir - $awal;
	$hari = floor($datediff/(60*60*24))+1;
	$bulan = ceil($hari/30);
	return $bulan;
}

function periodeSewaHari($awal,$akhir){
	$awal = strtotime($awal);
	$akhir = strtotime($akhir);
	$datediff = $akhir - $awal;
	$hari = floor($datediff/(60*60*24))+1;
	return $hari;
}

function getStatus($awal,$akhir){ // FORMAT Y-m-d
	if( strtotime($akhir) >= strtotime(date('Y-m-d')) && strtotime(date('Y-m-d')) >= strtotime($awal)) $status="Open";
	else if ( strtotime(date('Y-m-d')) < strtotime($akhir) ) $status="Booked";
	else $status="Closed";
	return $status;
}

function terbilang($x){
	$arr = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
	if ($x < 12)
	return " " . $arr[$x];
	elseif ($x < 20)
	return terbilang($x - 10) . " Belas";
	elseif ($x < 100)
	return terbilang($x / 10) . " Puluh" . terbilang($x % 10);
	elseif ($x < 200)
	return " Seratus" . terbilang($x - 100);
	elseif ($x < 1000)
	return terbilang($x / 100) . " Ratus" . terbilang($x % 100);
	elseif ($x < 2000)
	return " Seribu" . terbilang($x - 1000);
	elseif ($x < 1000000)
	return terbilang($x / 1000) . " Ribu" . terbilang($x % 1000);
	elseif ($x < 1000000000)
	return terbilang($x / 1000000) . " Juta" . terbilang($x % 1000000);
}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* untuk melakukan check apakah sudah ada user yang login.
bila belum, redirect ke login page.
berguna agar halaman page tidak dapat diakses ketika user belum login. */
function check_logged_admin($username) {
	$loginUrl = base_url('admin/dashboard/login');
    if(!$username) {
        if(current_url() != $loginUrl) {
            redirect('admin/dashboard/login');
            exit();
        }
    }
    else {
        if(current_url() == $loginUrl) {
            redirect('admin/dashboard');
            exit();
        }
    }
}

// check if user is logged in
function isLogin($user){
    if(!$user){
        redirect('/');
    }
    // return $this->session->userdata('name');
}
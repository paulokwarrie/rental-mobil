<?php class Invoice_model extends CI_Model {

	private $table='invoice';

	function __construct() {
		parent::__construct();
	}
	
	function get($id=false){
		if($id) $query = $this->db->get_where($this->table, array('id' => $id));
		else $query = $this->db->get($this->table);
		return $query;
	}

	function get_where($data,$limit=false,$offset=false){
		$query = $this->db->get_where($this->table, $data, $limit, $offset);
		return $query;
	}

	function add($data){
		$this->db->insert($this->table, $data);
		if($this->db->affected_rows() > 0) return "Success";
		else return "Fail";
	}
	
	function edit($id,$data){
		$this->db->update($this->table, $data, array('id' => $id));
		if($this->db->affected_rows() > 0) return "Success";
		else return "Fail";
	}
	
	function delete($id){
		$this->db->delete($this->table, array('id' => $id)); 
		if($this->db->affected_rows() > 0) return "Success";
		else return "Fail";
	}

} ?>
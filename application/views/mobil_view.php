<div class="row">
	<div class="col-xs-12" style="margin-top:10px">
		<?=printConfirmationMsg('success')?>
		<?=printConfirmationMsg('fail', 'danger')?>
		<?=validation_errors('<div class="alert fade in alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>','</div>')?>
	</div>

	<div class="col-xs-12">
		<p class="text-right">
			<?=anchor('javascript:;', 'Add Record', array('class' => 'btn btn-primary btn-sm', 'data-toggle' => 'modal', 'data-target' => '#myModal'))?>
		</p>
		<div class="table-responsive">
			<table id="mobil" class="table table-striped table-hover display nowrap cell-border" cellspacing="0" width="100%" style="font-size:12px">
				<thead>
					<tr>
						<th>Merk/Jenis/Type</th>
						<th>No. Polisi</th>
						<th>Tahun Keluaran</th>
						<th>Warna</th>
						<th>Harga Sewa Per Bulan</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<?php if(isset($mobil)) { foreach($mobil->result_array() as $item): ?>
					<tr>
						<td><?=$item['merk_jenis_type']?></td>
						<td><?=$item['no_polisi']?></td>
						<td style="text-align:right"><?=$item['tahun_keluaran']?></td>
						<td><?=$item['warna']?></td>
						<td style="text-align:right"><span style="display:none"><?=sprintf("%020d", $item['harga_sewa_per_bulan'])?></span>Rp. <?=number_format($item['harga_sewa_per_bulan'],0, ',', '.')?></td>
						<td>
							<div class="dropdown">
								<a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog fa-fw"></i>
								</a>
								<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
									<!-- <li><?=anchor('mobil/edit_form/'.encryptURL($item['id']), '<i class="fa fa-pencil fa-fw"></i> Edit', array('title' => 'edit'))?></li> -->
									<li><?=anchor('mobil/delete/'.encryptURL($item['id']), '<i class="fa fa-trash fa-fw"></i> Delete', array('title' => 'edit', "onclick" => "return confirm('Apakah Anda yakin ingin menghapus record ini ? Segala Kontrak dan Invoice yang berhubungan dengan Mobil ini akan terhapus !')"))?></li>
								</ul>
							</div>
						</td>
					</tr>
					<?php endforeach; } ?>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Record</h4>
				</div>
				<?=form_open('mobil/add',array('class' => 'form-horizontal'));?>
					<div class="modal-body">
						<div class="form-group">
							<label for="merk_jenis_type" class="col-sm-4 control-label">Merk / Jenis / Type <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="merk_jenis_type" placeholder="Cth: Isuzu Panther / MT" required>
							</div>
						</div>
						<div class="form-group">
							<label for="no_polisi" class="col-sm-4 control-label">No. Polisi <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="no_polisi" required>
							</div>
						</div>
						<div class="form-group">
							<label for="tahun_keluaran" class="col-sm-4 control-label">Tahun Keluaran <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="tahun_keluaran" required>
							</div>
						</div>
						<div class="form-group">
							<label for="warna" class="col-sm-4 control-label">Warna <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="warna" required>
							</div>
						</div>
						<div class="form-group">
							<label for="harga_sewa_per_bulan" class="col-sm-4 control-label">Harga Sewa per Bulan <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="harga_sewa_per_bulan" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				<?=form_close();?>
			</div>
		</div>
	</div>

</div>
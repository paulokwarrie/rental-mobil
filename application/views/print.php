<!DOCTYPE html>
<html lang="en">

	<head>
		<meta name="robots" content="noindex">
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<title><?=$title?></title>
		<!-- Style -->
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/bootstrap-3.3.5-dist/css/bootstrap.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/metisMenu/dist/metisMenu.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/DataTables-1.10.8/media/css/jquery.dataTables.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/bootstrapAdmin/css/sb-admin-2.css')?>">
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/font-awesome/css/font-awesome.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/select2-master/dist/css/select2.min.css')?>">
		<!-- <link rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css"> -->
		<style type="text/css" media="print">
			.background {
				background-color: #000000 !important;
				-webkit-print-color-adjust: exact; 
			}
		</style>
	</head>

	<body style="-webkit-print-color-adjust:exact">
		<?php
			if(isset($invoice)) {
				foreach($invoice->result() as $invoice):
					foreach($invoice->kontrak->result() as $kontrak):
						foreach($kontrak->pelanggan->result() as $pelanggan):
		?>
		<div class="row">
			<div class="col-xs-12">
				<table width="100%">
					<tbody>
						<tr>
							<td>No. Invoice</td>
							<td>:</td>
							<td><?=$invoice->no_invoice?></td>
							<td>No. Kontrak</td>
							<td>:</td>
							<td><?=$kontrak->no_kontrak?></td>
						</tr>
						<tr>
							<td>Periode Sewa</td>
							<td>:</td>
							<td><?=$invoice->periode_sewa?></td>
							<td>Pelanggan</td>
							<td>:</td>
							<td><?=$pelanggan->nama_perusahaan?></td>
						</tr>
						<tr>
							<td>Tanggal Invoice</td>
							<td>:</td>
							<td><?=date('d/m/Y',strtotime($invoice->tgl_terbit))?></td>
							<td>No. Telp</td>
							<td>:</td>
							<td><?=$pelanggan->no_telp_perusahaan?></td>
						</tr>
						<tr>
							<td>Tanggal Jatuh Tempo</td>
							<td>:</td>
							<td><?=date('d/m/Y',strtotime($invoice->jth_tempo))?></td>
							<td>Kota</td>
							<td>:</td>
							<td><?=$pelanggan->kota?></td>
						</tr>
					</tbody>
				</table>
				<br/>
				<table class="table table-bordered table-condensed" width="100%">
					<thead>
						<tr>
							<th style="text-align:center">No</th>
							<th style="text-align:center">Keterangan</th>
							<th style="text-align:center">Harga</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; foreach($invoice->mobil->result() as $mobil): $i++; ?>
						<tr>
							<td align="center"><?=$i;?></td>
							<td><?=$mobil->merk_jenis_type?></td>
							<td style="text-align:right"><?=number_format($mobil->harga_sewa_per_bulan,0, ',', '.')?></td>
						</tr>
						<?php endforeach; ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td style="text-align:right">Jumlah</td>
							<td style="text-align:right"><?=number_format($invoice->jumlah_pembayaran,0, ',', '.')?></td>
						</tr>
						<tr>
							<td></td>
							<td style="text-align:right">Pajak Ppn 10%</td>
							<td style="text-align:right"><?=number_format((0.1*$invoice->jumlah_pembayaran),0, ',', '.')?></td>
						</tr>
						<tr class="background" style="background-color:black;color:white;-webkit-print-color-adjust:exact;">
							<td></td>
							<td style="text-align:right"><b>Jumlah Total yang harus dibayar</b></td>
							<td style="text-align:right"><b><?=number_format(($invoice->jumlah_pembayaran+(0.1*$invoice->jumlah_pembayaran)),0, ',', '.')?></b></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i>Terbilang</i></b></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i><?=terbilang($invoice->jumlah_pembayaran+(0.1*$invoice->jumlah_pembayaran))?> Rupiah</i></b></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<?php
						endforeach;
					endforeach;
				endforeach;
			}
		?>
	</body>
</html>
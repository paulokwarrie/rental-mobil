<?php foreach($invoice->result() as $invoice) { ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">Invoice - <?=$invoice->no_invoice?></h4>
</div>
<?=form_open('pembayaran/add',array('class' => 'form-horizontal'))?>
<div class="modal-body table-responsive">
	<?php if(isset($invoice->pembayaran)) { ?>
		<h4><u>Histori Pembayaran</u></h4>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Jumlah Pembayaran</th>
					<th>Tanggal Pembayaran</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($invoice->pembayaran->result() as $pembayaran) { ?>
				<tr>
					<td style="text-align:right">Rp. <?=number_format($pembayaran->jumlah_pembayaran,0, ',', '.')?></td>
					<td style="text-align:right"><?=date('d-m-Y',strtotime($pembayaran->tgl_pembayaran))?></td>
					<td>
						<div class="dropdown">
							<a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-cog fa-fw"></i>
							</a>
							<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
								<li><?=anchor('pembayaran/delete/'.encryptURL($pembayaran->id), '<i class="fa fa-trash fa-fw"></i> Delete', array('title' => 'delete', "onclick" => "return confirm('Apakah Anda yakin ingin menghapus record ini ?')"))?></li>
							</ul>
						</div>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php if($invoice->kekurangan>0) { ?>
		<p align="right">Jumlah Kekurangan : Rp. <?=number_format($invoice->kekurangan,0, ',', '.')?></p>
		<?php } ?>
	<?php } ?>
	<?php if($invoice->total_pembayaran<$invoice->total_penjualan) { ?>
	<h4><u>Add Pembayaran</u></h4>
	<input type="hidden" class="form-control" name="id_invoice" value="<?=$invoice->id?>">
	<div class="form-group">
		<label for="tgl_pembayaran" class="col-sm-3 control-label">Tanggal Pembayaran <font color="red">*</font></label>
		<div class="col-sm-9">
			<input type="date" class="form-control" name="tgl_pembayaran" id="tgl_pembayaran" value="<?=date('Y-m-d')?>">
		</div>
	</div>
	<div class="form-group">
		<label for="jumlah_pembayaran" class="col-sm-3 control-label">Jumlah Pembayaran <font color="red">*</font></label>
		<div class="col-sm-9">
			<input type="number" class="form-control" name="jumlah_pembayaran" id="jumlah_pembayaran" min=1000 max=<?=$invoice->kekurangan?>>
		</div>
	</div>
	<?php } ?>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<?php if($invoice->total_pembayaran<$invoice->total_penjualan) { ?>
	<button type="submit" class="btn btn-primary">Add</button>
	<?php } ?>
</div>
<?=form_close();?>
<?php } ?>
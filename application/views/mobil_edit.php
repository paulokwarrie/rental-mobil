<div class="row">
	<div class="col-lg-12">
		<h2 class="page-header" style="margin-top:10px"><?=$title?></h2>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
	<div class="col-xs-12">
		<?=printConfirmationMsg('success')?>
		<?=printConfirmationMsg('fail', 'danger')?>
		<?=validation_errors('<div class="alert fade in alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>','</div>')?>
	</div>
	<!-- /.col-lg-12 -->
	<div class="col-xs-12">
		<?=form_open('mobil/edit/'.encryptURL($mobil['id']),array('class' => 'form-horizontal'));?>
			<div class="form-group">
				<label for="merk_jenis_type" class="col-sm-3 control-label">Merk / Jenis / Type <font color="red">*</font></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="merk_jenis_type" value="<?=$mobil['merk_jenis_type']?>" placeholder="Cth : Isuzu Panther / MT" required>
				</div>
			</div>
			<div class="form-group">
				<label for="no_polisi" class="col-sm-3 control-label">No. Polisi <font color="red">*</font></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="no_polisi" value="<?=$mobil['no_polisi']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="tahun_keluaran" class="col-sm-3 control-label">Tahun Keluaran <font color="red">*</font></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="tahun_keluaran" value="<?=$mobil['tahun_keluaran']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="warna" class="col-sm-3 control-label">Warna <font color="red">*</font></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="warna" value="<?=$mobil['warna']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="harga_sewa_per_bulan" class="col-sm-3 control-label">Harga Sewa per Bulan <font color="red">*</font></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="harga_sewa_per_bulan" value="<?=$mobil['harga_sewa_per_bulan']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-sm-3 control-label"></label>
				<div class="col-sm-9">
					<button type="submit" class="btn btn-primary">Edit</button>
					<a href="<?=base_url('mobil');?>" class="btn btn-default">Back</a>
				</div>
			</div>
		<?=form_close();?>
	</div>
	<!-- /.col-xs-12 -->
</div>
<!-- /.row -->
<div class="row">
	<div class="col-xs-12" style="margin-top:10px">
		<?=printConfirmationMsg('success')?>
		<?=printConfirmationMsg('fail', 'danger')?>
		<?=validation_errors('<div class="alert fade in alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>','</div>')?>
	</div>

	<div class="col-xs-12">
		<p class="text-right">
			<?=anchor('javascript:;', 'Add Record', array('class' => 'btn btn-primary btn-sm', 'data-toggle' => 'modal', 'data-target' => '#myModal'))?>
		</p>
		<div class="table-responsive">
			<table id="pelanggan" class="table table-striped table-hover display nowrap cell-border" cellspacing="0" width="100%" style="font-size:12px">
				<thead>
					<tr>
						<th>Nama Perusahaan</th>
						<th>Nama Penanggung Jawab</th>
						<th>No. Telp Perusahaan</th>
						<th>Kota</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<?php if(isset($pelanggan)) { foreach($pelanggan->result_array() as $item): ?>
					<tr>
						<td><?=$item['nama_perusahaan']?></td>
						<td><?=$item['nama_penanggung_jawab']?></td>
						<td><?=$item['no_telp_perusahaan']?></td>
						<td><?=$item['kota']?></td>
						<td>
							<div class="dropdown">
								<a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog fa-fw"></i>
								</a>
								<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
									<li><?=anchor('pelanggan/edit_form/'.encryptURL($item['id']), '<i class="fa fa-pencil fa-fw"></i> Edit', array('title' => 'edit'))?></li>
									<li><?=anchor('pelanggan/delete/'.encryptURL($item['id']), '<i class="fa fa-trash fa-fw"></i> Delete', array('title' => 'edit', "onclick" => "return confirm('Apakah Anda yakin ingin menghapus record ini ?')"))?></li>
								</ul>
							</div>
						</td>
					</tr>
					<?php endforeach; } ?>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Record</h4>
				</div>
				<?=form_open('pelanggan/add',array('class' => 'form-horizontal'));?>
					<div class="modal-body">
						<div class="form-group">
							<label for="nama_perusahaan" class="col-sm-4 control-label">Nama Perusahaan <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_perusahaan" required>
							</div>
						</div>
						<div class="form-group">
							<label for="nama_penanggung_jawab" class="col-sm-4 control-label">Nama Penanggung Jawab <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_penanggung_jawab" required>
							</div>
						</div>
						<div class="form-group">
							<label for="no_telp_perusahaan" class="col-sm-4 control-label">No. Telp Perusahaan <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="no_telp_perusahaan" required>
							</div>
						</div>
						<div class="form-group">
							<label for="kota" class="col-sm-4 control-label">Kota <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kota" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				<?=form_close();?>
			</div>
		</div>
	</div>

</div>
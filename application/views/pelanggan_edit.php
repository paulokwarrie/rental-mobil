<div class="row">
	<div class="col-lg-12">
		<h2 class="page-header" style="margin-top:10px"><?=$title?></h2>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
	<div class="col-xs-12">
		<?=printConfirmationMsg('success')?>
		<?=printConfirmationMsg('fail', 'danger')?>
		<?=validation_errors('<div class="alert fade in alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>','</div>')?>
	</div>
	<!-- /.col-lg-12 -->
	<div class="col-xs-12">
		<?=form_open('pelanggan/edit/'.encryptURL($pelanggan['id']),array('class' => 'form-horizontal'));?>
			<div class="form-group">
				<label for="nama_perusahaan" class="col-sm-3 control-label">Nama Perusahaan <font color="red">*</font></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="nama_perusahaan" value="<?=$pelanggan['nama_perusahaan']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="nama_penanggung_jawab" class="col-sm-3 control-label">Nama Penanggung Jawab <font color="red">*</font></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="nama_penanggung_jawab" value="<?=$pelanggan['nama_penanggung_jawab']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="no_telp_perusahaan" class="col-sm-3 control-label">No. Telp Perusahaan <font color="red">*</font></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="no_telp_perusahaan" value="<?=$pelanggan['no_telp_perusahaan']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="kota" class="col-sm-3 control-label">Kota <font color="red">*</font></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="kota" value="<?=$pelanggan['kota']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="harga_sewa_per_bulan" class="col-sm-3 control-label"></label>
				<div class="col-sm-9">
					<button type="submit" class="btn btn-primary">Edit</button>
					<a href="<?=base_url('pelanggan');?>" class="btn btn-default">Back</a>
				</div>
			</div>
		<?=form_close();?>
	</div>
	<!-- /.col-xs-12 -->
</div>
<!-- /.row -->
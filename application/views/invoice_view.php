<div class="row">
	<div class="col-xs-12" style="margin-top:10px">
		<?=printConfirmationMsg('success')?>
		<?=printConfirmationMsg('fail', 'danger')?>
		<?=validation_errors('<div class="alert fade in alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>','</div>')?>
	</div>

	<div class="col-xs-12">
		<p class="text-right">
			<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Add Record</a>
		</p>
		<div class="table-responsive">
			<table id="invoice" class="table table-striped table-hover display nowrap cell-border" cellspacing="0" width="100%" style="font-size:12px">
				<thead>
					<tr>
						<th>No. Invoice</th>
						<th>Periode Sewa</th>
						<th>Pelanggan</th>
						<th>Tanggal Terbit</th>
						<th>Jatuh Tempo</th>
						<th>Jumlah Pembayaran (+10% PPN)</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<?php if(isset($invoice)) { foreach($invoice->result() as $invoice): ?>
					<tr>
						<td><a href="<?=base_url('invoice/show_invoice/'.$invoice->id);?>" data-toggle="modal" data-target="#ajaxModal"><?=$invoice->no_invoice?></a></td>
						<td><?=$invoice->periode_sewa;?></td>
						<td>
							<a href="<?=base_url('invoice/show_pelanggan/'.$invoice->id);?>" data-toggle="modal" data-target="#ajaxModal"><?=$invoice->pelanggan_nama_perusahaan?></a>
						</td>
						<td><span style="display:none"><?=$invoice->tgl_terbit?></span><?=date("d-m-Y", strtotime($invoice->tgl_terbit));?></td>
						<td><span style="display:none"><?=$invoice->jth_tempo?></span><?=date("d-m-Y", strtotime($invoice->jth_tempo));?></td>
						<td style="text-align:right"><span style="display:none"><?=sprintf("%020d", $invoice->jumlah_penjualan)?></span>Rp. <?=number_format($invoice->jumlah_penjualan,0, ',', '.')?></td>
						<td><a href="<?=base_url('invoice/show_pembayaran_form/'.encryptURL($invoice->id))?>" data-toggle="modal" data-target="#ajaxModal"><?=$invoice->status?></a></td>
						<td>
							<div class="dropdown">
								<a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog fa-fw"></i>
								</a>
								<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
									<!-- <li><?=anchor('invoice/edit_form/'.encryptURL($invoice->id), '<i class="fa fa-pencil fa-fw"></i> Edit', array('title' => 'edit'))?></li> -->
									<li><?=anchor('invoice/print_view/'.encryptURL($invoice->id), '<i class="fa fa-print fa-fw"></i> Print', array('title' => 'print'))?></li>
									<li><?=anchor('invoice/delete/'.encryptURL($invoice->id), '<i class="fa fa-trash fa-fw"></i> Delete', array('title' => 'delete', "onclick" => "return confirm('Apakah Anda yakin ingin menghapus record ini ?')"))?></li>
								</ul>
							</div>
						</td>
					</tr>
					<?php endforeach; } ?>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Add Record Modal -->
	<div class="modal fade" id="myModal" tabindex="" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Record</h4>
				</div>
				<?=form_open('invoice/add',array('class' => 'form-horizontal'));?>
					<div class="modal-body">
						<div class="form-group">
							<label for="no_invoice" class="col-sm-4 control-label">No. Invoice <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="no_invoice" value="<?=$no_invoice?>" required>
							</div>
						</div>
						<div class="form-group">
							<label for="id_kontrak" class="col-sm-4 control-label">Kontrak <font color="red">*</font></label>
							<div class="col-sm-8">
								<select id="invoice_kontrak" name="id_kontrak" class="form-control select2" style="width:100%" required>
									<option value="">-</option>
									<?php if(isset($kontrak)) { foreach($kontrak->result() as $kontrak) { ?>
									<option value="<?=$kontrak->id?>"><?=$kontrak->no_kontrak.' | '.$kontrak->nama_perusahaan.' | '.$kontrak->awal_sewa.' s/d '.$kontrak->akhir_sewa?></option>
									<?php } } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="id_kontrak_to_periode" class="col-sm-4 control-label">Periode <font color="red">*</font></label>
							<div class="col-sm-8">
								<select id="invoice_periode" data-url="<?=base_url('kontrak/getPeriode/');?>" name="id_kontrak_to_periode" class="form-control select2" style="width:100%" required></select>
							</div>
						</div>
						<div class="form-group">
							<label for="tgl_terbit" class="col-sm-4 control-label">Tanggal Terbit <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="date" class="form-control" name="tgl_terbit" value="<?=date('Y-m-d')?>" min="<?=date('Y-m-d')?>" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				<?=form_close();?>
			</div>
		</div>
	</div>

	<!-- AJAX Modal -->
	<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Record</h4>
				</div>
				<?=form_open('pelanggan/add',array('class' => 'form-horizontal'));?>
					<div class="modal-body">
						<div class="form-group">
							<label for="nama_perusahaan" class="col-sm-4 control-label">Nama Perusahaan <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_perusahaan" required>
							</div>
						</div>
						<div class="form-group">
							<label for="nama_penanggung_jawab" class="col-sm-4 control-label">Nama Penanggung Jawab <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_penanggung_jawab" required>
							</div>
						</div>
						<div class="form-group">
							<label for="no_telp_perusahaan" class="col-sm-4 control-label">No. Telp Perusahaan <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="no_telp_perusahaan" required>
							</div>
						</div>
						<div class="form-group">
							<label for="kota" class="col-sm-4 control-label">Kota <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kota" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				<?=form_close();?>
			</div>
		</div>
	</div>

</div>
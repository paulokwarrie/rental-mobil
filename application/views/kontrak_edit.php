<div class="row">
	<div class="col-lg-12">
		<h2 class="page-header" style="margin-top:10px"><?=$title?></h2>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
	<div class="col-xs-12">
		<?=printConfirmationMsg('success')?>
		<?=printConfirmationMsg('fail', 'danger')?>
		<?=validation_errors('<div class="alert fade in alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>','</div>')?>
	</div>
	<!-- /.col-lg-12 -->
	<div class="col-xs-12">
		<?php foreach($kontrak->result() as $kontrak) { ?>
		<?=form_open('kontrak/edit/'.encryptURL($kontrak->id),array('class' => 'form-horizontal'));?>
			<div class="form-group">
				<label for="no_kontrak" class="col-sm-2 control-label">No. Kontrak <font color="red">*</font></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="no_kontrak" value="<?=$kontrak->no_kontrak?>" placeholder="Cth: 001/RM/09-2015" required>
				</div>
			</div>
			<div class="form-group">
				<label for="id_pelanggan" class="col-sm-2 control-label">Pelanggan <font color="red">*</font></label>
				<div class="col-sm-10">
					<select name="id_pelanggan" class="form-control select2" style="width:100%" required>
						<?php if(isset($pelanggan)) { foreach($pelanggan->result_array() as $pelanggan) { ?>
						<option value="<?=$pelanggan['id']?>" <?php if($pelanggan['id']==$kontrak->pelangganSelected) echo "selected='selected'" ?> ><?=$pelanggan['nama_perusahaan']?></option>
						<?php } } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="id_mobil" class="col-sm-2 control-label">Mobil <font color="red">*</font></label>
				<div class="col-sm-10">
					<select name="id_mobil[]" class="form-control select2" style="width:100%" multiple="multiple" required>
						<?php if(isset($mobil)) { foreach($mobil->result_array() as $mobil) { ?>
						<option value="<?=$mobil['id']?>"
							<?php foreach($kontrak->mobilSelected->result() as $mobs) {
								if($mobil['id']==$mobs->id) {
									echo "selected='selected'";
									break;
								}
							} ?>
						><?=$mobil['merk_jenis_type']." - ".$mobil['no_polisi']?></option>
						<?php } } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="awal_sewa" class="col-sm-2 control-label">Awal Sewa <font color="red">*</font></label>
				<div class="col-sm-10">
					<input type="date" class="form-control" name="awal_sewa" value="<?=$kontrak->awal_sewa?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="akhir_sewa" class="col-sm-2 control-label">Akhir Sewa <font color="red">*</font></label>
				<div class="col-sm-10">
					<input type="date" class="form-control" name="akhir_sewa" value="<?=$kontrak->akhir_sewa?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-sm-2 control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-primary">Edit</button>
					<a href="<?=base_url('kontrak');?>" class="btn btn-default">Back</a>
				</div>
			</div>
		<?=form_close();?>
		<?php } ?>
	</div>
	<!-- /.col-xs-12 -->
</div>
<!-- /.row -->
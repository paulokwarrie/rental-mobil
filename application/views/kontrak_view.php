<div class="row">
	<div class="col-xs-12" style="margin-top:10px">
		<?=printConfirmationMsg('success')?>
		<?=printConfirmationMsg('fail', 'danger')?>
		<?=validation_errors('<div class="alert fade in alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>','</div>')?>
	</div>

	<div class="col-xs-12">
		<p class="text-right">
			<?=anchor('#', 'Add Record', array('class' => 'btn btn-primary btn-sm', 'data-toggle' => 'modal', 'data-target' => '#myModal'))?>
		</p>
		<div class="table-responsive">
			<table id="kontrak" class="table table-striped table-hover display nowrap cell-border" cellspacing="0" width="100%" style="font-size:12px">
				<thead>
					<tr>
						<th>No. Kontrak</th>
						<th>Pelanggan</th>
						<th>Total Harga Sewa</th>
						<th>Awal Sewa</th>
						<th>Akhir Sewa</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<?php if(isset($kontrak)) { foreach($kontrak->result() as $kontrak): ?>
					<tr>
						<td><a href="<?=base_url('kontrak/show_mobil/'.$kontrak->id);?>" data-toggle="modal" data-target="#ajaxModal"><?=$kontrak->no_kontrak?></a></td>
						<td><a href="<?=base_url('kontrak/show_pelanggan/'.$kontrak->id);?>" data-toggle="modal" data-target="#ajaxModal"><?=$kontrak->nama_perusahaan?></a></td>
						<td style="text-align:right"><span style="display:none"><?=sprintf("%020d", $kontrak->total_harga_sewa)?></span>Rp. <?=number_format($kontrak->total_harga_sewa,0, ',', '.')?></td>
						<td><span style="display:none"><?=$kontrak->awal_sewa?></span><?=date("d-m-Y", strtotime($kontrak->awal_sewa));?></td>
						<td><span style="display:none"><?=$kontrak->akhir_sewa?></span><?=date("d-m-Y", strtotime($kontrak->akhir_sewa));?></td>
						<td>
							<?php
							if(is_null($kontrak->keterangan) || empty($kontrak->keterangan)){
								if(getStatus($kontrak->awal_sewa,$kontrak->akhir_sewa)=="Open") echo '<a href="'.base_url('kontrak/show_status/'.$kontrak->id).'" data-toggle="modal" data-target="#ajaxModal">Open</a>';
								else if(getStatus($kontrak->awal_sewa,$kontrak->akhir_sewa)=="Booked") echo '<a href="'.base_url('kontrak/show_status/'.$kontrak->id).'" data-toggle="modal" data-target="#ajaxModal">Booked</a>';
								else echo getStatus($kontrak->awal_sewa,$kontrak->akhir_sewa);
							} else echo '<a href="'.base_url('kontrak/show_keterangan/'.$kontrak->id).'" data-toggle="modal" data-target="#ajaxModal">Canceled</a>'; ?></a></td>
						<td>
							<div class="dropdown">
								<a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog fa-fw"></i>
								</a>
								<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
									<!-- <li><?=anchor('kontrak/edit_form/'.encryptURL($kontrak->id), '<i class="fa fa-pencil fa-fw"></i> Edit', array('title' => 'edit'))?></li> -->
									<li><?=anchor('kontrak/delete/'.encryptURL($kontrak->id), '<i class="fa fa-trash fa-fw"></i> Delete', array('title' => 'delete', "onclick" => "return confirm('Apakah Anda yakin ingin menghapus record ini ? Segala Invoice yang berhubungan dengan kontrak ini akan terhapus !')"))?></li>
								</ul>
							</div>
						</td>
					</tr>
					<?php endforeach; } ?>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Add Record Modal -->
	<div class="modal fade" id="myModal" tabindex="" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Record</h4>
				</div>
				<?=form_open('kontrak/add',array('class' => 'form-horizontal'));?>
					<div class="modal-body">
						<div class="form-group">
							<label for="no_kontrak" class="col-sm-4 control-label">No. Kontrak <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="no_kontrak" value="<?=$no_kontrak?>" placeholder="Cth: 001/RM/09-2015" required>
							</div>
						</div>
						<div class="form-group">
							<label for="id_pelanggan" class="col-sm-4 control-label">Pelanggan <font color="red">*</font></label>
							<div class="col-sm-8">
								<select name="id_pelanggan" class="form-control select2" style="width:100%" required>
									<?php if(isset($pelanggan)) { foreach($pelanggan->result_array() as $pelanggan) { ?>
									<option value="<?=$pelanggan['id']?>"><?=$pelanggan['nama_perusahaan']?></option>
									<?php } } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="awal_sewa" class="col-sm-4 control-label">Awal Sewa <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="date" class="form-control" id="kontrak_awal_sewa" name="awal_sewa" min="<?=date('Y-m-d')?>" required>
							</div>
						</div>
						<div class="form-group">
							<label for="akhir_sewa" class="col-sm-4 control-label">Akhir Sewa <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="date" class="form-control" id="kontrak_akhir_sewa" name="akhir_sewa" min="<?=date('Y-m-d')?>" required>
							</div>
						</div>
						<div class="form-group">
							<label for="id_mobil" class="col-sm-4 control-label">Mobil <font color="red">*</font></label>
							<div class="col-sm-8">
								<select data-url="<?=base_url('');?>" name="id_mobil[]" class="form-control select2" style="width:100%" multiple="multiple" required>
									<?php if(isset($mobil)) { foreach($mobil->result_array() as $mobil) { ?>
									<option value="<?=$mobil['id']?>"><?=$mobil['merk_jenis_type']." - ".$mobil['no_polisi']?></option>
									<?php } } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				<?=form_close();?>
			</div>
		</div>
	</div>

	<!-- AJAX Modal -->
	<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Record</h4>
				</div>
				<?=form_open('pelanggan/add',array('class' => 'form-horizontal'));?>
					<div class="modal-body">
						<div class="form-group">
							<label for="nama_perusahaan" class="col-sm-4 control-label">Nama Perusahaan <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_perusahaan" required>
							</div>
						</div>
						<div class="form-group">
							<label for="nama_penanggung_jawab" class="col-sm-4 control-label">Nama Penanggung Jawab <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="nama_penanggung_jawab" required>
							</div>
						</div>
						<div class="form-group">
							<label for="no_telp_perusahaan" class="col-sm-4 control-label">No. Telp Perusahaan <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="no_telp_perusahaan" required>
							</div>
						</div>
						<div class="form-group">
							<label for="kota" class="col-sm-4 control-label">Kota <font color="red">*</font></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="kota" required>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				<?=form_close();?>
			</div>
		</div>
	</div>

</div>
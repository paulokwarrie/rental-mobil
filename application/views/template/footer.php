	<!-- jQuery -->
    <script src="<?=base_url('_assets/plugins/jquery/dist/jquery.min.js')?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url('_assets/plugins/bootstrap-3.3.5-dist/js/bootstrap.min.js')?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=base_url('_assets/plugins/metisMenu/dist/metisMenu.min.js')?>"></script>
	
    <!-- DataTables Plugin JavaScript -->
    <script src="<?=base_url('_assets/plugins/DataTables-1.10.8/media/js/jquery.dataTables.min.js')?>"></script>
    <!-- DataTables Responsive Plugin JavaScript -->
    <script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

    <!-- Select2 JavaScript -->
    <script src="<?=base_url('_assets/plugins/select2-master/dist/js/select2.min.js')?>"></script>
	
    <!-- Custom Theme JavaScript -->
    <script src="<?=base_url('_assets/plugins/bootstrapAdmin/js/sb-admin-2.js')?>"></script>
	
	<script>
		$(document).ready(function() {
			// init Modal
			$('#myModal').on('shown.bs.modal', function () {
				$('#myInput').focus();
			});
			// init AJAX Modal
			$("#ajaxModal").on("shown.bs.modal", function(e) {
				var link = $(e.relatedTarget);
				$(this).find(".modal-content").load(link.attr("href"),null,checkStatus);
			});
			// init DataTables
			$('#mobil').DataTable({
				"aoColumns": [
				/* merk_jenis_type */ null,
				/* no_polisi */ null,
				/* tahun_keluaran */ null,
				/* warna */ null,
				/* harga_sewa_per_bulan */ null,
				/* action */ { "bSortable": false, "bSearchable": false, "bVisible": true },
				]
			});
			$('#pelanggan').DataTable({
				"aoColumns": [
				/* nama_perusahaan */ null,
				/* nama_penanggung_jawab */ null,
				/* no_telp_perusahaan */ null,
				/* kota */ null,
				/* action */ { "bSortable": false, "bSearchable": false, "bVisible": true },
				]
			});
			$('#kontrak').DataTable({
				"aoColumns": [
				/* no_kontrak */ null,
				/* pelanggan */ null,
				/* total_harga_sewa */ null,
				/* awal_sewa */ null,
				/* akhir_sewa */ null,
				/* status */ null,
				/* action */ { "bSortable": false, "bSearchable": false, "bVisible": true },
				]
			});
			$('#invoice').DataTable({
				"aoColumns": [
				/* no_invoice */ null,
				/* periode_sewa */ null,
				/* pelanggan */ null,
				/* tanggal_terbit */ null,
				/* jatuh_tempo */ null,
				/* jumlah_pembayaran */ null,
				/* status */ null,
				/* action */ { "bSortable": false, "bSearchable": false, "bVisible": true },
				]
			});
			// init Select2
			$(".select2").select2();
			// AJAX Select2
			$('#invoice_kontrak').change(function(){
				var id_kontrak = $(this).val();
				var result = null;
				$.ajax({
					url: $("#invoice_periode").data('url')+"/"+id_kontrak,
					type: "POST",
					dataType: "json",
					success: function(res, status){
						result = res;
						$("#invoice_periode").html("");
						$("#invoice_periode").select2({
							data: result
						});
					},
					error: function(xhr, ex, thrownError){
						// console.log("error : " + ex);
						// console.log(xhr.status);
						// console.log(xhr.responseText);
						// console.log(thrownError);
					},
					complete: function(xhr, status){
						//console.log("complete");
					}
				});
			});

			function formatResult(data) {
				console.log(data);
				if (data.loading) return data.text;
				if (data.awal_periode) {
					var markup = "<div>"+data.awal_periode+" s/d "+data.akhir_periode+"</div>";
				}
				return markup;
			}

			function formatResultSelection (data) {
				console.log(data);
				return data.awal_periode+" s/d "+data.akhir_periode || data.text;
			}

			// code
			function checkStatus(){
				var status = $("#status");
				var modal_body = status.closest('.modal-body');
				var keterangan = modal_body.find('#keterangan');
				if(status.val() == "Open") keterangan.attr('disabled','disabled');
				if(status.val() == "Booked") keterangan.attr('disabled','disabled');
				status.on('change', function() {
					if(status.val() == "Canceled") keterangan.prop('disabled', false);
					else keterangan.prop('disabled', true);
				});
			}

			// function isEmpty(str) {
			// 	return (!str || 0 === str.length);
			// }

			// $('#kontrak_awal_sewa, #kontrak_akhir_sewa').change(function(){
			// 	if($('#kontrak_awal_sewa').val().length > 0 && $('#kontrak_akhir_sewa').val().length>0){
			// 		// do something
			// 	}
			// });

			// function initSelect2AvailableCar(){
			// 	var id_kontrak = $(this).val();
			// 	var result = null;
			// 	$.ajax({
			// 		url: $("#invoice_periode").data('url')+"/"+id_kontrak,
			// 		type: "POST",
			// 		dataType: "json",
			// 		success: function(res, status){
			// 			result = res;
			// 			$("#invoice_periode").html("");
			// 			$("#invoice_periode").select2({
			// 				data: result
			// 			});
			// 		},
			// 		error: function(xhr, ex, thrownError){
			// 			// console.log("error : " + ex);
			// 			// console.log(xhr.status);
			// 			// console.log(xhr.responseText);
			// 			// console.log(thrownError);
			// 		},
			// 		complete: function(xhr, status){
			// 			//console.log("complete");
			// 		}
			// 	});
			// }

		} );
	</script>
	
	</body>
</html>
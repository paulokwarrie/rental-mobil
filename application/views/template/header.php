<!DOCTYPE html>
<html lang="en">

	<head>
		<meta name="robots" content="noindex">
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<title><?=$title?></title>
		<!-- Style -->
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/bootstrap-3.3.5-dist/css/bootstrap.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/metisMenu/dist/metisMenu.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/DataTables-1.10.8/media/css/jquery.dataTables.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_assets/plugins/bootstrapAdmin/css/sb-admin-2.css')?>">
        <link rel="stylesheet" href="<?=base_url('_assets/plugins/font-awesome/css/font-awesome.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('_assets/plugins/select2-master/dist/css/select2.min.css')?>">
        <!-- <link rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css"> -->
	</head>

	<body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?=base_url()?>"><i class="fa fa-car fa-fw"></i> Rental Mobil</a>
                </div>
                <!-- /.navbar-header -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="<?=base_url('mobil')?>"><i class="fa fa-car fa-fw"></i> Mobil</a>
                            </li>
                            <li>
                                <a href="<?=base_url('pelanggan')?>"><i class="fa fa-users fa-fw"></i> Pelanggan</a>
                            </li>
                            <li>
                                <a href="<?=base_url('kontrak')?>"><i class="fa fa-book fa-fw"></i> Kontrak</a>
                            </li>
                            <li>
                                <a href="<?=base_url('invoice')?>"><i class="fa fa-list-alt fa-fw"></i> Invoice</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">